#! /usr/bin/env python3

# Standard library imports
import json
import os
import re

# Related third party imports
import configargparse
import gitlab
import requests


# Constant variables
GITLAB_GRAPHQL_QUERY = """
query {{
    project(fullPath: "{project_path}") {{
        pipelines(ref: "master" after: "{cursor}") {{
            nodes {{
                iid
                sha
            }}
            pageInfo {{
                endCursor
                hasNextPage
            }}
        }}
    }}
}}
"""


def parse_options():
    parser = configargparse.ArgumentParser()

    parser.add_argument("--package-name", env_var="PACKAGE_NAME", required=True)
    parser.add_argument("--package-version", env_var="PACKAGE_VERSION", required=True)
    parser.add_argument("--toolchain-name", env_var="TOOLCHAIN_NAME", default="ubuntu-18.04-x86_64")
    parser.add_argument("--package-requirements", env_var="PACKAGE_REQUIREMENTS", required=True)

    parser.add_argument("--gitlab-url", env_var="GITLAB_URL", required=True)
    parser.add_argument("--gitlab-token", env_var="GITLAB_TOKEN", required=True)
    parser.add_argument("--gitlab-group", env_var="GITLAB_GROUP", default="irystec")

    return parser.parse_args()


def get_artifact_versions(artifact_paths):
    versions = {}
    for path in artifact_paths:
        for artifact in os.listdir(path):
            # Get version information from artifact
            version_info = {}
            if artifact.endswith(".apk"):
                # Get from apk file name
                pass
            elif artifact.endswith(".jar"):
                # Get from jar file name
                pass
            else:
                with open(os.path.join(path, artifact), "br") as artifact_file:
                    artifact_content = artifact_file.read()

                if artifact.endswith(".glsl"):
                    # Check for comment
                    pass
                else:
                    re_search = re.search(rb"__IRYS__(?P<component>\w+-\w+) (?P<version>\d+\.\d+\.\d+) "
                                          + rb"\(toolchain-(?P<tc_name>.+): (?P<tc_version>\d+)\)__IRYS__",
                                          artifact_content,
                                          )
                    if re_search is None:
                        raise RuntimeError("ERROR: Can't get version info from artifact: " + os.path.join(path,
                                                                                                          artifact,
                                                                                                          ),
                                           )

                # Save version info if not already known
                if re_search.group('component') not in versions:
                    versions[re_search.group('component')] = {'version': re_search.group('version'),
                                                              'toolchain_name': re_search.group('tc_name'),
                                                              'toolchain_version': re_search.group('tc_version'),
                                                              }

    return versions


def get_sha_from_pipeline_iid(gitlab_url, gitlab_token, project_path, pipeline_iid):
    cursor = ""
    while True:
        # Get a list of pipelines
        response = requests.post(os.path.join(gitlab_url, "api/graphql"),
                                 headers={'Authorization': "Bearer " + gitlab_token},
                                 json={'query': GITLAB_GRAPHQL_QUERY.format(project_path=project_path,
                                                                            cursor=cursor,
                                                                            ),
                                       },
                                 )
        response.raise_for_status()

        response_json = response.json()
        for pipeline in response_json['data']['project']['pipelines']['nodes']:
            if pipeline['iid'] == pipeline_iid:
                return pipeline['sha']

        # Set cursor to query next page of pipelines
        cursor = response_json['data']['project']['pipelines']['pageInfo']['endCursor']

        if not response_json['data']['project']['pipelines']['pageInfo']['hasNextPage']:
            raise RuntimeError("ERROR: Can't find pipeline with IID {} in project {}".format(pipeline_iid,
                                                                                             project_path,
                                                                                             ),
                               )


def main(options):
    print("INPUTS:")
    print("    Package name: " + options.package_name)
    print("    Package version: " + options.package_version)
    print("    Toolchain name: " + options.toolchain_name)
    print("    Package requirements: " + options.package_requirements)
    print()

    print("Connecting to GitLab... ", end="", flush=True)
    gitlab_client = gitlab.Gitlab(options.gitlab_url, options.gitlab_token)
    gitlab_client.http_get("/version")
    print("Done")

    # Generate conanfile
    # Either from env or manifest (could be incomplete?)

    # Package resources

    # Generate manifest
    # Get commit SHA from pipeline IID

    # Add tags to GitLab


if __name__ == "__main__":
    main(parse_options())


# conan install --update --profile TOOLCHAIN_NAME conanfile.txt
# __IRYS__(?P<component>w+-w+)

# re_match = re.match(
#     r"(?P<feature>[^_]+)_(?P<edge>(No)?Edge)_(?P<lite>(No)?Lite)_(?P<fps>\d+FPS)_(?P<img>[^_]+)\.(csv|json)",
#     filename,
#     )

"""
query {
  project(fullPath: "gitlab-job-exec/gitlab-job-exec") {
    pipelines(ref: "master" after: "") {
      nodes {
        iid
        sha
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
}
query { project(fullPath: \"jfblain/test-project\") {avatarUrl}}

query { 
  project(fullPath: "jfblain/test-project") {
    avatarUrl
  }
}
"""
