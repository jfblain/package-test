#
# Temporary image used to download & build dependencies
#
FROM registry.gitlab.com/jfblain/package-test:tc-ubuntu AS builder

# Custom content
COPY bin /tmp/bin
RUN for i in /tmp/bin/*; do mv $i ${i%.*}; done


#
# Main image
#
FROM registry.gitlab.com/jfblain/package-test:tc-ubuntu

# Custom content
COPY --from=builder /tmp/bin /usr/local/bin

# Needed dependencies
COPY requirements.txt /tmp/requirements.txt
RUN pip3 install --cache-dir /tmp/pip --requirement /tmp/requirements.txt && \
    rm --recursive --force /tmp/pip /tmp/requirements.txt

# Default command
CMD /bin/bash
